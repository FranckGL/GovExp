# Dépense publique

Ce repo contient différents travaux sur la dépense publique. Tout est parti du souhait de mettre à jour et d'étendre (en terme de pays et d'année) les travaux de  [Gouardo Lenglart](https://www.strategie.gouv.fr/publications/reduire-poids-de-depense-publique) sur les dépenses publiques. Actuellement, le projet est structuré autour de 4 documents:

* 'Contruction_DépensesAPU.Rmd' reprend les calculs de Gouardo-Lenglart;
* 'Examen_DépensesPubliques.Rmd' examine (succinctement, pour l'instant) les résultats;
* 'D62 D632 FR.Rmd' examine les prestations sociales en France (en cours);
* 'NotesDépensesPubliques.Rmd' contient quelques notes personnelles (encore très lacunaires) sur le sujet des dépenses publiques, et entreprend de synthétiser quelques papiers sur le sujet. (Pas sûr que j'arrive à dégager bcp de temps pour travailler sur ce document...)
* 'Skyline.Rmd' fait le graphique emblématique, qui synthétise l'analyse.
* 'SkylineCPS.Rmd' reprend le principe du graphique 'Skyline', mais porte uniquement sur les dépenses de protection sociale, avec les principaux risques.

Licence : Ces programmes sont régis par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/