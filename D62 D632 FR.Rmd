---
title: "D62 D632 en GF10"
author: "Franck"
date: "25/12/2021"
output: html_document
---

author: "Franck"
date: "`r format(Sys.time(), '%d %B %Y à %Hh%M')`"
output:
  word_document:
    toc: yes
    number_sections: yes
  html_document:
    toc: yes
    number_sections: yes
---

La France ne fournit pas le détail du D62 D632 avant 2009. C'est assez ennuyeux pour la décomposition de Gouardo Lenglart. Cette note examine spécifiquement ces dépenses pour le poste GF10 'Protection sociale'.

```{r include=FALSE}
library(dplyr)
library(ggplot2)
library(eurostat)
library(flextable)

knitr::opts_chunk$set(echo = FALSE,
                      warning = FALSE,
                      message = FALSE,
                      fig.width = 8,
                      fig.height = 4
)

set_flextable_defaults(big.mark = " ",
                       decimal.mark = ",",
                       table.layout ="autofit",
                       font.size = 10,
                       padding = 0)

# Pour affichage
fR <- function(x,d=0) format(round(x,d),big.mark = "&nbsp;",decimal.mark = ",",nsmall=d)
fN <- function(n) prettyNum(n,big.mark = "&nbsp;")
```


```{r}
nama_10_gdp <- get_eurostat(id = "nama_10_gdp", time="num") 

pib <- nama_10_gdp %>% 
  filter(na_item == "B1GQ",
         unit == "CP_MEUR") %>% 
  select(-na_item,-unit) %>% 
  rename(PIB = values)

#gov_10a_exp <- get_eurostat(id = "gov_10a_exp", time_format = "num")
# saveRDS(object = gov_10a_exp %>% 
#           filter(
#     time >= 1995,
#     unit == "MIO_EUR"
#   ), file = "C:/Users/franck.arnaud/Desktop/Dépense publique/gov_10a_exp.rds")

gov_10a_exp <- readRDS(file = "C:/Users/franck.arnaud/Desktop/GovExp/gov_10a_exp.rds")
```


```{r}
govexp <- gov_10a_exp %>% 
  filter(
    time >= 1995,
    geo == "FR",
    na_item %in% c("D62","D632","D62_D632"),
    substr(cofog99,1,4)=="GF10",
    unit == "MIO_EUR",
    !is.na(values)
  ) %>% 
  select(-unit)
```

La table ci-dessous rappelle la nomenclature de dépenses de protection sociale dans cofog.
```{r 'Nomenclature GF10'}
nomenclatureGF10 <- govexp %>% 
  distinct(cofog99) %>% 
  left_join(get_eurostat_dic(dictname = "cofog99", lang = "fr"),
            by  = c("cofog99" = "code_name"))

nomenclatureGF10 %>% 
  flextable()
```

# Examen de la composition des prestations de protection sociale

Les dépenses de Vieillesse (GF1002) représentent un peu plus de la moitié des dépenses publiques de protection sociale. Je ne suis pas sûr de bien comprendre l'augmentation de 2 points de la part de ces dépenses en 2004...
```{r}
zz <- nomenclatureGF10 %>% pull(full_name)
names(zz) <- nomenclatureGF10 %>% pull(cofog99)


govexp %>% 
  filter(sector == "S13",
         na_item == "D62_D632",
         nchar(cofog99)==6) %>% 
  group_by(time) %>% 
  mutate(Part = values/sum(values)) %>% 
  ungroup() %>% 
  ggplot(aes(x=time,y = Part*100, fill = cofog99)) + geom_col(position = position_stack()) +
  scale_y_continuous(limits = c(0,100), expand = expansion(mult= rep(0,2))) +
  scale_x_continuous(expand = expansion(mult= rep(0.01,2))) +
  scale_fill_discrete(labels = zz) +
  theme(legend.pos= "top",
        axis.ticks.x = element_blank()) +
  guides(fill = guide_legend(ncol = 3,byrow=TRUE)) +
  geom_text(aes(label = ifelse(Part>0.01,fR(Part*100,1),NA_character_)), position = position_stack(vjust = 0.5), size=2.5) +
  labs(x="", 
       y = "Part (en %)",
       fill = "",
       title = "Composition des prestations de protection sociale")
```

