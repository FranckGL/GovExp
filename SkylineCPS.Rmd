---
title: "Skyline des dépenses sociales"
author: "Franck"
date: "`r format(Sys.time(), '%d %B %Y à %Hh%M')`"
output:
  word_document:
  html_document:
  toc: yes
number_sections: yes
---
  
Ce document propose une comparaison des dépenses de protection sociale publiques entre la France et des pays européens, adaptée de celle qui figure dans  [Où réduire le poids de la dépense publique](https://www.strategie.gouv.fr/publications/reduire-poids-de-depense-publique)

```{r include=FALSE}
library(dplyr)
library(ggplot2)
library(eurostat)
library(flextable)

knitr::opts_chunk$set(echo = FALSE,
                      warning = FALSE,
                      message = FALSE,
                      fig.width = 8,
                      fig.height = 6
)

set_flextable_defaults(big.mark = " ",
                       decimal.mark = ",",
                       table.layout ="autofit",
                       font.size = 10,
                       padding = 0)

# Pour affichage
fR <- function(x,d=0) format(round(x,d),big.mark = "&nbsp;",decimal.mark = ",",nsmall=d)
fN <- function(n) prettyNum(n,big.mark = "&nbsp;")
```


# Construction des données

Chargement du PIB et du PIB potentiel...
```{r "Charge PIB potentiel"}
#PIBpotentiel <- readRDS(file = "PIBpotentiel/PIB potentiel - HP.rds")
library(seee)
data(pib)

pib <- pibval %>% select(-PIB,-country)
```

OK !


Chargement données SESPROS...
```{r "Charge SESPROS"}
spr_exp_sum <- get_eurostat(id = "spr_exp_sum", time_format = "num")

#saveRDS(object = "spr_exp_sum", file = "spr_exp_sum 31 mars 2022.rds")

# Nomenclature de dépenses
nom_spdeps <- left_join(spr_exp_sum %>% distinct(spdeps),
                        get_eurostat_dic(dictname="spdeps", lang="fr") %>%
                          rename(spdeps=code_name,
                                 nom=full_name),
                        by="spdeps"
)

# Dépenses normalisées par le PIB potentiel
cps_eu <- spr_exp_sum %>% 
  filter(unit == "MIO_EUR",
         !is.na(values)) %>% 
  select(-unit) %>% 
  left_join(pib, by = c("geo","time")) %>% 
  mutate(valuespib = values/PIBpot*100) %>% 
  select(-PIBpot)
```


# Calculs
Je colle aux risques de SESPROS : vieillesse, survie, maladie, etc.

On compare la dépense moyenne (relativement au PIB potentiel) en France à celle de 11 autres pays européens. Attention, à partir de 2019 (inclus), la comparaison ne porte que sur 10 pays, car les données du Royaume-Uni ne sont plus disponibles à partir de 2019 (à cause du Brexit).

```{r}
# Moyenne 11 pays
moyenne11 <- cps_eu %>% 
  filter(geo %in% c("DE","UK","AT","BE","NL","IT","ES","PT","FI","SE","DK"),
         time >= 1995) %>% 
  group_by(spdeps,time) %>% 
  summarise(valuespib = mean(valuespib),
            n = n(),
            .groups = "drop")
# On n'a pas le Royaume-Uni en 2019

# France et écarts
base0 <- cps_eu %>% 
  filter(geo=="FR") %>% 
  rename(FR = valuespib) %>% 
  select(-values) %>% 
  inner_join(moyenne11 %>% rename(Moy = valuespib), by = c("spdeps","time")) %>% 
  mutate(Delta = FR - Moy)
```

# Skyline


```{r "Préparation données"}


# J'ai un peu changé l'ordre de la nomenclature, pour avoir la santé juste après les retraites
Nomenclature <- tibble::tribble(
  ~spdeps,  ~Garde, ~Ordre, ~Intitulé,
  "ADMIN", 1, 10, "Dépenses de\nfonctionnement",
  "OLDSURVIV",0 , 1 , "Vieillesse et survie",
  "OLD", 1 , 1.1 , "Vieillesse",
  "SURVIV",1 , 1.2 , "Survie",
  "SICKDISA",0, 2, "Maladie, soins de santé\n et invalidité",
  "SICK",1, 2.1, "Maladie et\nsoins de santé",
  "DISA",1, 2.2, "Invalidité",
  "UNEMPLOY",1, 3, "Emploi",
  "FAM",1, 4, "Famille",
  "HOUSEEXCLU", 0,5, "Logement et\n exclusion sociale",
  "EXCLU", 1,5.1, "Exclusion sociale",
  "HOUSE", 1,6, "Logement",
  "OTHER", 0,9, "Autres dépenses"
)


base <- base0 %>% 
  left_join(Nomenclature,by="spdeps") %>% 
  filter(Garde>0.5) %>% 
  arrange(time,Ordre)
```


```{r}
tableau <- function(ANNEE) {
  # Tableau
  ff <- base %>% 
    filter(geo == "FR",time == ANNEE) %>% 
    arrange(Ordre) %>%
    select(-spdeps,-n, -time, -Ordre) %>% 
    mutate(FR = round(FR,1),
           Moy = round(Moy,1),
           Delta = round(Delta,1)) %>% 
    flextable() %>% 
    add_header_lines(values = paste("Structure des dépenses de protection sociale en France en",ANNEE)) %>% 
    style(part = "header",pr_p = officer::fp_par(text.align = "center"),
          pr_t = officer::fp_text(bold=TRUE))
  
  return(ff)
}

skyline <- function(ANNEE, title = TRUE) {
  espacement <- 0.1
  
  prepa <- base %>% 
    filter(time == ANNEE) %>% 
    arrange(Ordre) %>% 
    mutate(xmax = cumsum(Moy),
           xmin = c(0,xmax[-n()]) + espacement*(1:n()-1),
           xmax = xmax + espacement*(1:n()-1),
           ymin = 0,
           ymax = FR/Moy)
  
  gg <- prepa %>% 
    ggplot(aes(xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax, fill = spdeps)) +
    geom_rect() +
    geom_hline(yintercept = 1, linetype = "dashed", col = "grey50") +
    geom_segment(aes(x = 0,
                     y = 0.15,
                     xend = prepa %>% slice(1) %>% pull(xmax),
                     yend = 0.15),
                 arrow = arrow(ends = "both",length = unit(0.3, "cm"))) +
    scale_x_continuous(expand = expansion(mult = rep(0.01,2)),
                       breaks = prepa %>% mutate(xx = 0.5*(xmin+xmax)) %>% pull(xx),
                       labels = prepa %>% pull(Intitulé)) +
    scale_y_continuous(expand = expansion(mult = c(0,0.05))) +
    scale_fill_manual(values = met.brewer(name="Hiroshige",n=12)) +
    geom_text(aes(x = 0.5*(xmin+xmax), y = 0,
                  label = formatC(round(Moy,1), format="f", digits = 1,  decimal.mark = ",")),
              vjust = -0.2) +
    geom_text(aes(x = 0.5*(xmin+xmax),
                  y = FR/Moy,
                  label = formatC(round(FR-Moy,1), flag="+", format="f", digits = 1,  decimal.mark = ",")),
              vjust = -0.2) +
    geom_text(aes(x = prepa %>% slice(1) %>% pull(xmax)/2,
                  y = 0.15,
                  label = "Moyenne\neuropéenne"),
              size=3, vjust = -0.2) +
      theme(axis.ticks.x = element_blank(),
          axis.text.x = element_text(angle = 45, hjust = 1),
          axis.ticks.y = element_blank(),
          axis.text.y = element_blank(),
          legend.pos = "none",
          panel.background = element_rect(fill = "white")) +
    labs(x = "",
         y = "",
         title = if (isTRUE(title)) paste("Structure des dépenses de protection sociale en France et comparaison européenne,", ANNEE) else NULL)  
  
  return(gg)
}

library(MetBrewer)

skyline(2016);tableau(2016)
skyline(2017);tableau(2017)
skyline(2018);tableau(2018)
skyline(2019);tableau(2019)
skyline(2020);tableau(2020)
#skyline(2019, title = FALSE)

# Pour 2020 :
#   1) On ne peut pas faire le graphique avec la moyenne si on inclut le Royaume-Uni, pour lequel on n'a pas les données !
#   2) Les dépenses de l'année 2020 sont de nature exceptionnelle
```

# Comparaison par poste

## Retraite
Ici, on se met sur le champ droits directs + droits dérivés + minimum vieillesse.
```{r}
spr_exp_fol <- get_eurostat(id = "spr_exp_fol", time_format = "num")
spr_exp_fsu <- get_eurostat(id = "spr_exp_fsu", time_format = "num")

DD <- spr_exp_fol %>%
  filter(unit=="MIO_EUR",
         spscheme == "TOTAL",
         spdep %in% c("SNTCPOLDPEN","SNTCPANTPEN","SNTCPPARPEN","SNTCLUMP",
                      "STACPOLDPEN","STACPANTPEN","STACPPARPEN","STACLUMP")
  ) %>% 
  group_by(geo,time) %>% 
  summarise(values = sum(values),
            .groups = "drop")

DR <- spr_exp_fsu %>%
  filter(unit=="MIO_EUR",
         spscheme == "TOTAL",
         spdep %in% c("SNTCASH")
  ) %>% 
  group_by(geo,time) %>% 
  summarise(values = sum(values),
            .groups = "drop")

Retr <- bind_rows(DD,DR) %>% 
  group_by(geo,time) %>% 
  summarise(values = sum(values),
            .groups = "drop") %>% 
  left_join(pib,
            by = c("geo","time")) %>% 
  mutate(valuespib = values/PIBpot*100) %>% 
  select(-PIBpot)

# Moyenne 11 
moyenne11 <- Retr %>% 
  filter(geo %in% c("DE","UK","AT","BE","NL","IT","ES","PT","FI","SE","DK"),
         !is.na(values),
         time >= 1995) %>% 
  group_by(time) %>% 
  summarise(valuespib = mean(valuespib),
            n = n(),
            .groups = "drop")

# France et écarts
base0 <- Retr %>% 
  filter(geo=="FR") %>% 
  rename(FR = valuespib) %>% 
  select(-values) %>% 
  inner_join(moyenne11 %>% rename(Moy = valuespib), by = c("time")) %>% 
  mutate(Delta = FR - Moy)
```