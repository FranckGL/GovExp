---
title: "Skyline des dépenses publiques"
author: "Franck"
date: "`r format(Sys.time(), '%d %B %Y à %Hh%M')`"
output:
  word_document:
  html_document:
    toc: yes
    number_sections: yes
---

Ce document propose une comparaison des dépenses publiques entre la France et 11 pays de l'UE, adaptée de celle qui figure dans  [Où réduire le poids de la dépense publique](https://www.strategie.gouv.fr/publications/reduire-poids-de-depense-publique).

La première partie décrit de façon synthétique la méthode retenue. La seconde présente les skylines pour les années 2016 à 2021, dans une logique de robustesse, bien que la crise sanitaire limite les comparaisons des années 2020 et 2021 aux années antérieures. Enfin, la troisième partie illustre l'évolution des écarts entre la France et les pays de référence, pour chaque poste de dépenses considéré.


 
```{r include=FALSE}
library(dplyr)
library(ggplot2)
library(eurostat)
library(flextable)

knitr::opts_chunk$set(echo = FALSE,
                      warning = FALSE,
                      message = FALSE,
                      fig.width = 8,
                      fig.height = 6
)

set_flextable_defaults(big.mark = " ",
                       decimal.mark = ",",
                       table.layout ="autofit",
                       font.size = 10,
                       padding = 0)

# Pour affichage
fR <- function(x,d=0) format(round(x,d),big.mark = "&nbsp;",decimal.mark = ",",nsmall=d)
fN <- function(n) prettyNum(n,big.mark = "&nbsp;")
```




# Méthode
A partir de la nomenclature COFOG, on crée une typologie des dépenses publiques. Pour chaque année et différents pays, on détermine le poids des dépenses dans le PIB, et on compare ce chiffre pour la France à celui d'une moyenne (non pondérée) de pays. Tout ensemble de pays est envisageable; ici, on a choisi un agrégat constitué de 11 pays : Allemagne, Royaume-uni, Espagne, Italie, Autriche, Belgique, Pays-Bas, Portugal, Finlande, Danemark et Suède.



Améliorations à apporter

* Au sein du poste "Accueil et aides à la personne", est-il possible distinguer l'accueil des personnes âgées et celui des enfants ?
* Préciser davantage ce qui est représenté




# Skylines 2016-2021
Les skylines sont assez proches d'une année à l'autre.


```{r "Préparation données"}
depenses0 <- readRDS(file = "Dépenses publiques.rds")

# J'ai un peu changé l'ordre de la nomenclature, pour avoir la santé juste après les retraites
Nomenclature <- tibble::tribble(
  ~Catégorie,  ~Garde, ~Ordre, ~Intitulé,
  "D1"  , 0, 10. , "Prestations de redistribution monétaire",
  "D11" , 1, 11. , "Retraite\net vieillesse",
  "D12" , 1, 12. , "Aides sociales",
  "D122", 0, 12.1, "   dont chômage",
  "D2"  , 0, 20. , "Fourniture de services sociaux et culturels",
  "D21" , 1, 11.1 , "Santé",
  "D211", 0, 11.11, "Hôpital public",
  "D212", 0, 11.12, "Santé marchande\net médicaments",
  "D22" , 1, 22. , "Education\net recherche",
  "D23" , 1, 23. , "Loisirs et culture",
  "D24" , 1, 12.2 , "Accueil et aides\nà la personne",
  "D3"  , 0, 30. , "Fourniture de services généraux",
  "D31" , 1, 31.,  "Défense,\npolice, justice",
  "D32" , 1, 32. , "Administration\ngénérale",
  "D321", 0, 32.1, "   dont APUC et ASSO",
  "D322", 0, 32.2, "   dont APUL",  
  "D4"  , 0, 40. , "Soutien à l'économie",
  "D41" , 1, 41., "Subventions et aides\nà l'économie",
  "D411", 0, 41.1, "   dont APUC et ASSO",
  "D412", 0, 41.2, "   dont APUL",
  "D42" , 1, 42. , "Investissement\n(hors social et régalien)",
  "D421", 0, 42.1, "   dont APUC et ASSO",
  "D422", 0, 42.2, "   dont APUL",
  "D5"  , 1, 50. , "Charge de la dette",
  "Somme", 0, 80., "Dépenses totales (somme)",
  "Total", 1, 90., "Dépenses totales"
)

# On conserve seulement les dépenses qu'on représentera dans le graphique
depenses <- depenses0 %>% 
  left_join(Nomenclature,by = "Catégorie") %>% 
  filter(Garde>0.5) %>% 
  select(-Garde)
  
# Calcul des moyennes de dépenses sur la Zone Euro hors France (soit 19-1 = 18 pays)
moyenneZE <- depenses %>% 
  filter(geo %in% c("DE", "AT", "BE", "ES", "FI", "IE", "IT", "LU", "NL", "PT",
                    "EL", "SI", "CY", "MT", "SK", "LT", "LV", "EE")) %>% 
  group_by(Catégorie, time) %>% 
  summarise(valuespib = mean(valuespib),
            n = n(),
            .groups = "drop")

# Calcul des moyennes sur les 11 pays de Gouardo-Lenglart (dont le Royaume-Uni)
GL11 <- c("DE", "AT", "BE", "ES", "FI", "IT", "NL", "PT", "DK", "SE", "UK")
moyenne11 <- depenses %>% 
  filter(geo %in% GL11) %>% 
  group_by(Catégorie, time) %>% 
  summarise(valuespib = mean(valuespib),
            n = n(),
            .groups = "drop")


# Au final, on retient la moyenne sur les 11 pays de Gouardo-Lenglart
moyenne <- moyenne11

# La variable n permet de vérifier que, pour chaque Catégorie et chaque année, on a bien fait la moyenne de 18 pays

base <- left_join(depenses %>% 
                    filter(geo=="FR") %>% 
                    select(Catégorie, Ordre, Intitulé, time, valuespib) %>% 
                    rename(FR = valuespib),
                  moyenne %>% rename(Moy = valuespib),
                  by  = c("Catégorie","time")
) %>% 
  select(-n) %>% 
  arrange(Ordre,time) %>% 
  filter(!is.na(FR)) 


# Préparation des statistiques pour le texte qui suit
zz <- base %>% 
  mutate(Ecart = round(FR-Moy,1)) %>% 
  filter(time>=2012) %>% 
  select(-FR,-Moy) %>% 
  arrange(time,Ordre)

zMin <- zz %>% filter(Catégorie=="Total") %>% summarise(min=min(Ecart)) %>% pull(min)
zMax <- zz %>% filter(Catégorie=="Total") %>% summarise(max=max(Ecart)) %>% pull(max)

AN <- 2021L

zRetr <- zz %>% filter(Catégorie=="D11",time == AN ) %>% pull(Ecart)
zSubv <- zz %>% filter(Catégorie=="D41",time== AN) %>% pull(Ecart)
zAidesSoc <- zz %>% filter(Catégorie=="D12",time == AN) %>% pull(Ecart)
zSante <- zz %>% filter(Catégorie=="D21",time == AN) %>% pull(Ecart)
```


La dépense publique est sensiblement supérieure en France à celle des pays de référence, dans une proportion variable d'une année à l'autre; entre 2012 et 2021, le supplément de dépense varie entre `r fR(zMin,1)` et `r fR(zMax,1)` points de PIB. Quatre postes de dépenses concentrent l'essentiel des écarts :

* la retraite (écart de `r fR(zRetr,1)` point en `r AN`);
* les subventions et aides à l'économie (écart de `r fR(zSubv,1)` pt en `r AN`);
* les aides sociales (écart de `r fR(zAidesSoc,1)` pt en `r AN`);
* la santé (écart de `r fR(zSante,1)` pt en `r AN`).

La crise sanitaire induit une forte hausse des dépenses de santé, d'ampleur comparable en France et dans les pays de référence.

Les dépenses de 'Subventions et aides à l'économie' augmentent de façon importante en 2019, à cause du double compte du CICE.




```{r}
zz %>% 
  tidyr::pivot_wider(names_from=time,values_from=Ecart) %>% 
  select(-Catégorie,-Ordre) %>% 
  flextable %>% 
    add_header_lines(values = "Ecart de dépense publique en France") %>% 
    style(part = "header",pr_p = officer::fp_par(text.align = "center"),
          pr_t = officer::fp_text(bold=TRUE))
```


```{r "Définition des fonctions pour les skylines"}
tableau <- function(ANNEE) {
   # Tableau
  zz <- base %>% 
    filter(time == ANNEE) %>% 
    arrange(Ordre) %>%
    select(-Catégorie,-time, -Ordre)
  
  ff <- zz %>% 
    mutate(Intitulé = gsub(pattern = "\\\n",replacement= " ", x = Intitulé),
           Delta = FR-Moy,
           FR = round(FR,1),
           Moy = round(Moy,1),
           Delta = round(Delta,1)) %>% 
    flextable %>% 
    add_header_lines(values = paste("Structure de la dépense publique en France en",ANNEE)) %>% 
    style(part = "header",pr_p = officer::fp_par(text.align = "center"),
          pr_t = officer::fp_text(bold=TRUE)) %>% 
    bold(i=nrow(zz),part = "body")
  
  return(ff)
}


# Cette fonction écrit la part des dépenses d'ordre social; je ne l'utilise pas dans la suite (c'était pratique)
texte <- function(ANNEE) {
  zz <- base %>% 
    filter(time == ANNEE,
           Catégorie %in% paste0("D",c(11,12,21,24))) %>% 
    summarise(FR=sum(FR),
              Moy = sum(Moy))
  
  zFR <- zz %>% pull(FR)
  zMoy <- zz %>% pull(Moy)
  
  zFRtot <- base %>% filter(time==ANNEE,Catégorie == "Total") %>% pull(FR)
  txt <- paste0("Les dépenses publiques d'ordre social représentent de l'ordre de ",round(zFR)," points sur les ",round(zFRtot,0)," points de PIB de dépenses publiques ; parmi les autres pays considérés, ces dépenses représentent de l'ordre de ",round(zMoy)," points de PIB.")
  
  return(txt)
}


library(MetBrewer) # pour la fonction met.brewer -> palette de couleurs

skyline <- function(ANNEE, title = TRUE) {
  espacement <- 0.1
  
  prepa <- base %>% 
    filter(time == ANNEE,
           Catégorie != "Total") %>% 
    arrange(Ordre) %>% 
    mutate(xmax = cumsum(Moy),
           xmin = c(0,xmax[-n()]) + espacement*(1:n()-1),
           xmax = xmax + espacement*(1:n()-1),
           ymin = 0,
           ymax = FR/Moy)
  
  gg <- prepa %>% 
    ggplot(aes(xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax, fill = Catégorie)) +
    geom_rect() +
    geom_hline(yintercept = 1, linetype = "dashed", col = "grey50") +
    geom_segment(aes(x = 0,
                   y = 0.15,
                   xend = prepa %>% slice(1) %>% pull(xmax),
                   yend = 0.15),
               arrow = arrow(ends = "both",length = unit(0.3, "cm"))) +
    scale_x_continuous(expand = expansion(mult = rep(0.01,2)),
                       breaks = prepa %>% mutate(xx = 0.5*(xmin+xmax)) %>% pull(xx),
                       labels = prepa %>% pull(Intitulé)) +
    scale_y_continuous(expand = expansion(mult = c(0,0.05))) +
    scale_fill_manual(values = met.brewer(name="Hiroshige",n=12)) +
    geom_text(aes(x = 0.5*(xmin+xmax), y = 0,
                  label = formatC(round(Moy,1), format="f", digits = 1,  decimal.mark = ",")),
              vjust = -0.2) +
    geom_text(aes(x = 0.5*(xmin+xmax),
                  y = FR/Moy,
                  label = formatC(round(FR-Moy,1), flag="+", format="f", digits = 1,  decimal.mark = ",")),
              vjust = -0.2) +
    geom_text(aes(x = prepa %>% slice(1) %>% pull(xmax)/2,
                  y = 0.15,
                  label = "Moyenne\neuropéenne"),
              size=3, vjust = -0.2) +
    theme(axis.ticks.x = element_blank(),
          axis.text.x = element_text(angle = 45, hjust = 1),
          axis.ticks.y = element_blank(),
          axis.text.y = element_blank(),
          legend.pos = "none") +
    labs(x = "",
         y = "",
         title = if (isTRUE(title)) paste("Structure de la dépense publique en France et comparaison européenne,", ANNEE) else NULL)  
  
 return(gg)
}
```


```{r}
skyline(2016);tableau(2016)
skyline(2017);tableau(2017)
skyline(2018);tableau(2018)
skyline(2019);tableau(2019)
skyline(2020);tableau(2020)
skyline(2021);tableau(2021)
#skyline(2021, title = FALSE) # Pour note ministre
```



# Evolutions des dépenses publiques

## Dépenses publiques totales
Sur la figure ci-dessous, on représente les évolutions des différents postes, et les évolutions de l'écart.

```{r fig.height=10}
ordre <- base %>% distinct(Intitulé,Ordre) %>% pull(Intitulé)

depenses %>% 
  filter(geo %in% c("FR",GL11)) %>% 
  mutate(epaisseur = if_else(geo=="FR","France","Autres"),
         Intitulé=factor(Intitulé, levels=ordre,ordered=TRUE)
         ) %>% 
  ggplot(aes(x = time, y = valuespib, color = geo, size = epaisseur)) + geom_line() +
  scale_size_manual(values = c("France" = 2., "Autres" =1.)) +
  facet_wrap(Intitulé~., scales="free_y",ncol = 3) +
  guides(size=FALSE) +
  labs(x="",y= "En % de PIB",
       color = "Pays", 
       title="Evolution des dépenses publiques par poste de la nomenclature")
```


```{r}
zz <- base %>% 
  mutate(Ecart=FR-Moy) %>% 
  select(-FR, -Moy) %>% 
  mutate(Intitulé=factor(Intitulé, levels=ordre,ordered=TRUE))


zz %>% 
  filter(Catégorie=="Total") %>% 
  ggplot(aes(x=time,y=Ecart)) + geom_col() +
  geom_hline(yintercept = 0, col="black") +
  geom_text(aes(label=round(Ecart,1)),position = position_stack(vjust=0.5), size= 3) +
  scale_y_continuous(expand = expansion(mult = c(0,0.02))) +
  theme(axis.ticks.x = element_blank()) +
  labs(x="",y= "En % de PIB",
       title="Supplément de dépenses publiques totales de la France, par rapport aux pays de référence ")
```


```{r fig.height=10}
zz %>% 
  filter(Catégorie != "Total") %>% 
  ggplot(aes(x=time,y=Ecart,fill=Ecart)) + geom_col() +
  geom_hline(yintercept = 0, col="black") +
  facet_wrap(Intitulé~., ncol=3) +
  theme(axis.ticks.x = element_blank()) +
  labs(x="",y= "En % de PIB",
       title="Evolution de l'écart des dépenses publiques en France, par poste de la nomenclature")
```



## Dépenses publiques sociales
Les dépense publiques sociales regroupent 4 catégories de la nomenclature de dépense utilisée dans cette note : les retraites, la santé, les aides sociales et les prestations d'accueil et d'aides à la personne.

```{r "Fig - dépenses sociales FR"}
depenses %>% 
  filter(Catégorie %in% paste0("D",c(11,12,21,24)),
         geo=="FR",
         time >= 2009) %>% 
  mutate(Intitulé=factor(Intitulé, levels=ordre,ordered=TRUE)) %>% 
  ggplot(aes(x=time,y=valuespib,fill=Intitulé)) + geom_col(position = position_stack(reverse=TRUE)) +
  scale_y_continuous(expand = expansion(mult = c(0,0.1))) +
  scale_x_continuous(expand = expansion(mult = rep(0.01,2))) +
  scale_fill_manual(values = met.brewer(name="Monet")) +
  geom_text(aes(label=round(valuespib,1)), size = 3.5, position = position_stack(reverse=TRUE,vjust = 0.5)) +
  geom_text(aes( x = time, y = Somme, label = round(Somme,1), fill=NULL),
            data = . %>% group_by(time) %>% summarise(Somme = sum(valuespib),.groups="drop"),
            size = 3.5, vjust = -0.3) +
  theme(axis.ticks.x = element_blank(),
        legend.pos = "top")+
  labs(x="",
       y = "En % du PIB potentiel",
       fill = "",
       title = "Dépenses publiques sociales dans le PIB en France"
    )
```


```{r "Fig - comp inter dépenses sociales"}
zz <- depenses %>% 
  filter(Catégorie %in% paste0("D",c(11,12,21,24)),
         time >= 2021) %>% 
  mutate(Intitulé = factor(Intitulé, levels=ordre,ordered=TRUE))

virer_pas_complet <- c("BG","IS")

zz %>% 
  filter(!geo %in% virer_pas_complet) %>% 
  left_join(zz %>% group_by(geo) %>% summarise(Somme = sum(valuespib),.groups="drop"),by="geo") %>% 
  mutate(geo = reorder(geo,Somme)) %>% 
  ggplot(aes(x = geo, y = valuespib, fill = Intitulé)) +
  geom_col(position = position_stack(reverse=TRUE)) +
  scale_y_continuous(expand = expansion(mult = c(0,0.1))) +
  scale_fill_manual(values = met.brewer(name="Monet")) +
  geom_text(aes(label = round(valuespib,1)),
            size = 3.5,
            position = position_stack(reverse=TRUE,vjust = 0.5)) +
  geom_text(aes( y = Somme, label = round(Somme,1), fill=NULL),
            data = . %>% group_by(geo) %>% summarise(Somme = sum(valuespib),.groups="drop"),
            size = 3.5, vjust = -0.3) +
  theme(axis.ticks.x = element_blank(),
        legend.pos = "top")+
  labs(x="",
       y = "En % de PIB",
       fill = "",
       title = "Part des dépenses publiques sociales dans le PIB en 2021"
    )
```